#include "YourWidget.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

#define CODE_CODE             "c"
#define CODE_ID               "i"
#define CODE_KIDS             "k"
#define CODE_WRAP_CONTENT     "r"
#define CODE_TEXT_SIZE        "s"
#define CODE_WEIGHT           "w"
#define CODE_JUSTIFY          "j"
#define CODE_INIT_TEXT        "t"
#define CODE_CONNECTED_WIDGET "n"
#define CODE_CONNECTED_PARAM  "p"


WID RobotUIWidget::mWidgetCounter = FIRST_UNIQUE_WID;
GrowArray<RobotUIWidget, WID, WIDGET_CAPACITY_INCREMENT, ABSOLUTE_MAX> RobotUIWidget::mAllWidgets;

RobotUIWidget::RobotUIWidget()
{
  Init(false, 0);
}

RobotUIWidget::RobotUIWidget(WID widgetIndexOverride)
{
  Init(true, widgetIndexOverride);
}

RobotUIWidget::~RobotUIWidget()
{
  mAllWidgets.Set(mWidgetIndex, NULL);
}

void RobotUIWidget::Init(bool overrideTheIndex, WID overrideIndex)
{
  if(mWidgetCounter>=mAllWidgets.Capacity())
  {
    int requiredCapacity = mWidgetCounter+1;
    if(overrideTheIndex && (requiredCapacity < (overrideIndex+1)))
    {
      requiredCapacity = (overrideIndex+1);
    }
    mAllWidgets.EnsureCapacity(requiredCapacity);
  }
  //mWeight = 1;
  if(overrideTheIndex)
  {
    mWidgetIndex = overrideIndex;
    if(mWidgetCounter==overrideIndex)
    {
      mWidgetCounter++;
    }
  }
  else
  {
    mWidgetIndex = mWidgetCounter++;  
  }
  if(mWidgetIndex < ABSOLUTE_MAX)
  {
    mAllWidgets.Set(mWidgetIndex, this);  
  }
  else
  {
    //DEBUG_PRINTLN("Fatal Error, more than 255 widgets");
  }
}

void RobotUIWidget::AppendWidgetAsJSONStart(Stream& layoutStream, float weight, TextSize textSize, const char* initialText, int justification, RobotUI& robotUI)
{
  AppendWidgetAsJSONStart(layoutStream, GetCharCode(), GetWidgetIndexAsInt(), weight, textSize, initialText, justification, -1, 0, robotUI);
}

void RobotUIWidget::AppendWidgetAsJSONStart(Stream& layoutStream, char charCode, int widAsInt, float weight, TextSize textSize, const char* initialText, int justification, int connectedWidgetIndex, int connectedWidgetParam, RobotUI& robotUI)
{
  if(robotUI.GetStreamState() == LSS_CONT_WAITING_FOR_FIRST_CHILD)
  {
    layoutStream.print(",\"" CODE_KIDS "\":[");
    //layoutStream.print('[');
  }
  else if(robotUI.GetStreamState() == LSS_NODE_COMPLETE)
  {
    layoutStream.print(',');
  }
  layoutStream.print("{\"" CODE_CODE "\":\"");
  layoutStream.print(charCode);
  layoutStream.print("\"");
  if(widAsInt>=0)
  {
    layoutStream.print(",\"" CODE_ID "\":");
    layoutStream.print(widAsInt);
  }
  if(weight!=1.0f)
  {
    layoutStream.print(",\"" CODE_WEIGHT "\":");
    layoutStream.print(weight);
  }
  if(textSize!=DEFAULT_TEXT_SIZE)
  {
    layoutStream.print(",\"" CODE_TEXT_SIZE "\":");
    layoutStream.print(textSize);
  }
  if(initialText!=NULL)
  {
    layoutStream.print(",\"" CODE_INIT_TEXT "\":\"");
    layoutStream.print(initialText);
    layoutStream.print("\"");
  }
  if(justification!=DEFAULT_JUSTIFY)
  {
    layoutStream.print(",\"" CODE_JUSTIFY "\":");
    layoutStream.print(justification);
  }
  if(connectedWidgetIndex>=0)
  {
    layoutStream.print(",\"" CODE_CONNECTED_WIDGET "\":");
    layoutStream.print(connectedWidgetIndex);
    layoutStream.print(",\"" CODE_CONNECTED_PARAM "\":");
    layoutStream.print(connectedWidgetParam);
  }
  robotUI.SetStreamState(LSS_CONT_WAITING_FOR_FIRST_CHILD);
}

void RobotUIWidget::AppendWidgetAsJSONEnd(Stream& layoutStream, RobotUI& robotUI)
{
  int streamState = robotUI.GetStreamState();
  if(streamState == LSS_CONT_WAITING_FOR_FIRST_CHILD)
  {
    //layoutStream->print("[]");
  }
  else if(streamState == LSS_NODE_COMPLETE)
  {
    layoutStream.print(']');
  }
  layoutStream.print('}');
  robotUI.SetStreamState(LSS_NODE_COMPLETE);
}

