#ifndef ROBOTUISPINNER
#define ROBOTUISPINNER
#include "YourWidget.h"

class RobotUISpinner : public RobotUIWidget
{
  public:

  RobotUISpinner();

  virtual void SetOptions(char const * const * const options, char numOptions, char selectedOption);
  virtual void SetOptionsF(char const * const * const options PROGMEM, char numOptions, char selectedOption);
  void SetOptionEnabled(char optionIndex, bool enabled);
  void SetOption(char optionIndex);
  void ConsumeHasChanged();
  bool HasChanged();
  char GetOption();

  virtual char GetCharCode()
  {
    return CODE_SPINNER;
  }
  void SetOptionEnabledFromApp(char optionIndex, bool enabled);
  void SetOptionFromApp(char optionIndex);

  private:
  void SetOptionInternal(char optionIndex, bool commandFromApp, bool forceSend);
  void SetOptionEnabledInternal(char optionIndex, bool enabled, bool commandFromApp, bool forceSend);
  void SendState();

  char mSelectedOption;
  bool mEnabled;
  bool mHasChanged;
};

#endif // #ifndef ROBOTUISPINNER
