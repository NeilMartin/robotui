#include "RobotUIButton.h"
#include "RobotUICheckBox.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

RobotUICheckBox::RobotUICheckBox(TextSize textSize)
{
  mIsPressed = false;
  mHasChanged = false;
  mTextSize = textSize;
  mEnabled = true;
}

void RobotUICheckBox::SetName(const char* newName)
{
  if(newName != NULL)
  {
    RobotUI::CommandStart();
    RobotUI::CommandAppend(APP_CMD_SET_NAME);
    RobotUI::CommandAppend(GetWidgetIndexAsInt());
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(newName);
    RobotUI::CommandAppend("}");
    RobotUI::CommandEnd();
  }
}

void RobotUICheckBox::SetName(const __FlashStringHelper* newName)
{
  if(newName != NULL)
  {
    const int bufferSize = 64;
    char charBuffer[bufferSize];
    strncpy_P(charBuffer, (const char*)newName, bufferSize-1);
    charBuffer[bufferSize-1] = 0;
    SetName(charBuffer);
  }
}

bool RobotUICheckBox::IsPressed()
{
  return mIsPressed;
}

void RobotUICheckBox::SetIsPressed(bool pressed)
{
  SetIsPressedInternal(pressed, false, false);
}

void RobotUICheckBox::ForceIsPressed(bool pressed)
{
  SetIsPressedInternal(pressed, false, true);
}

void RobotUICheckBox::SetIsPressedFromApp(bool pressed)
{
  SetIsPressedInternal(pressed, true, false);
}

void RobotUICheckBox::SetIsPressedInternal(bool pressed, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if(mIsPressed != pressed)
  {
    mIsPressed = pressed;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUICheckBox::SetIsPressedEnabled(bool pressed, bool enabled)
{
  SetIsPressedEnabledInternal(pressed, enabled, false, false);
}

void RobotUICheckBox::SetIsPressedEnabledInternal(bool pressed, bool enabled, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if((mIsPressed != pressed) || (mEnabled != enabled))
  {
    mIsPressed = pressed;
    mEnabled = enabled;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUICheckBox::SendState()
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_CHECK_BOX);
  RobotUI::CommandAppend(mIsPressed?"1":"0");
  RobotUI::CommandAppend(mEnabled?"1":"0");
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandEnd();
}

void RobotUICheckBox::ConsumeHasChanged()
{
  mHasChanged = false;
}

bool RobotUICheckBox::HasChanged()
{
  return mHasChanged;
}

