#ifndef YOURWIDGET
#define YOURWIDGET
#include "GrowArray.h"

#define SPACE_WID           0
#define FIRST_UNIQUE_WID    1

#define JUSTIFY_TOP_LEFT     1
#define JUSTIFY_TOP          2
#define JUSTIFY_TOP_RIGHT    3
#define JUSTIFY_LEFT         4
#define JUSTIFY_CENTRE       5
#define JUSTIFY_CENTER       5
#define JUSTIFY_RIGHT        6
#define JUSTIFY_BOTTOM_LEFT  7
#define JUSTIFY_BOTTOM       8
#define JUSTIFY_BOTTOM_RIGHT 9

#define DEFAULT_TEXT_SIZE       15
#define DEFAULT_JUSTIFY       JUSTIFY_TOP_LEFT


#define CODE_VERT           'V'
#define CODE_HORIZ          'H'
#define CODE_VERT_SCROLL    'U'
#define CODE_HORIZ_SCROLL   'R'

#define CODE_BUTTON         'B'
#define CODE_CHECK_BOX      'C'
#define CODE_GREY_SPACE     'G'
#define CODE_SEEK_BAR       'K'
#define CODE_SPACE          ' '
#define CODE_SPINNER        'E'
#define CODE_SWITCH         'S'
#define CODE_TOGGLE_BUTTON  'T'
#define CODE_TEXT           'W'
#define CODE_UNKNOWN        'X'

#define APP_CMD_SET_NAME          "N"
#define APP_CMD_SET_DUAL_NAME     "M"
#define APP_CMD_SET_ENABLED       "H"
#define APP_CMD_SET_TOGGLE_BUTTON "T"
#define APP_CMD_SET_CHECK_BOX     "C"
#define APP_CMD_SET_SWITCH        "S"
#define APP_CMD_SET_SEEK_BAR      "E"
#define APP_CMD_SET_SPINNER       "P"
#define APP_CMD_SET_NAME_OPTIONS  "O"
#define APP_CMD_SET_TEXT          "W"
#define APP_CMD_APPEND_TEXT       "A"

typedef unsigned char WID;
typedef unsigned char WeightVal;
typedef unsigned char TextSize;
typedef unsigned char PressCount;
class RobotUI;

#define WIDGET_CAPACITY_INCREMENT 16
#define ABSOLUTE_MAX ((1 << (8*sizeof(WID)))-1)

class RobotUIWidget
{
  public:

  RobotUIWidget();
  RobotUIWidget(WID widgetIndexOverride);
  ~RobotUIWidget();

  int GetWidgetIndexAsInt()
  {
    return (int)mWidgetIndex;
  }
  WID GetWidgetIndexAsWID()
  {
    return mWidgetIndex;
  }
  virtual char GetCharCode()
  {
    return CODE_UNKNOWN;
  }
  virtual int GetChildCount()
  {
    return 0;
  }
  virtual WID GetChildWidgetID(WID index)
  {
    return 0;
  }
  
  void AppendWidgetAsJSONStart(Stream& layoutStream, float weight, TextSize textSize, const char* initialText, int justification, RobotUI& robotUI);
  static void AppendWidgetAsJSONStart(Stream& layoutStream, char charCode, int widAsInt, float weight, TextSize textSize, const char* initialText, int justification, int connectedWidgetIndex, int connectedWidgetParam, RobotUI& robotUI);
  static void AppendWidgetAsJSONEnd(Stream& layoutStream, RobotUI& robotUI);
  virtual void InsertWidgetAttribs(Stream& layoutStream)
  {
  }
  
  static RobotUIWidget* GetWidget(WID index)
  {
    return mAllWidgets[index];
  }

private:
  void Init(bool overrideTheIndex, WID overrideIndex);
  
  private:
  static WID mWidgetCounter;
  static GrowArray<RobotUIWidget, WID, WIDGET_CAPACITY_INCREMENT, ABSOLUTE_MAX> mAllWidgets;
  WID mWidgetIndex;
};

#endif // #ifndef YOURWIDGET
