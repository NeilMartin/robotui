#ifndef GROWARRAY
#define GROWARRAY
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

template<class T, class S, int INCREMENT, int MAX_SIZE>
class GrowArray
{
  public:

  GrowArray()
  {
    mCapacity = (S)INCREMENT;
    mSize = 0;
    mAllThings = (T**)calloc(INCREMENT, sizeof(T*));
    //mAllThings = new T*[INCREMENT];
  }
  
  ~GrowArray()
  {
    if(mAllThings != NULL)
    {
      free(mAllThings);
      //delete [] mAllThings;
    }
    mCapacity = 0;
    mSize = 0;
  }

  T* operator[](S index)
  {
    if((index >= 0) && (index < mSize))
    {
      return mAllThings[index];  
    }
    return NULL;
  }

  void Set( S index, T* thing )
  {
    if(index < mCapacity)
    {
      mAllThings[index] = thing;
      if(index+1>mSize)
      {
        mSize = index+1;
      }
    }
  }
  
  S Capacity()
  {
    return mCapacity;
  }
  
  S Size()
  {
    return mSize;
  }
  
  void Clear(int newCapacity)
  {
    mSize = 0;
    if(newCapacity>=1)
    {
      if(mCapacity != (S)newCapacity)
      {
        if(mAllThings!=NULL)
        {
          free(mAllThings);
        }
        mAllThings = (T**)calloc(newCapacity, sizeof(T*));
        mCapacity = (S)newCapacity;
      }
    }
  }
  
  void EnsureCapacity(int requiredCapacity)
  {
    int newCapacity = mCapacity;
    while(newCapacity < requiredCapacity)
    {
      newCapacity += INCREMENT;
      if(newCapacity >= MAX_SIZE)
      {
        newCapacity = MAX_SIZE;
        break;
      }
    }
    if(mCapacity != (S)newCapacity)
    {
      T** newThings = (T**)calloc(newCapacity, sizeof(T*));
      //T** newThings = new T*[newCapacity];
      if(newThings==NULL)
      {
        Serial.println(F("GrowArray failed"));
      }
      else
      {
        if(mAllThings!=NULL)
        {
          for(int i=0;i<mSize;++i)
          {
            newThings[i] = mAllThings[i];
          }
          free(mAllThings);
          //delete[] mAllThings;
        }
        mAllThings = newThings;
        mCapacity = (S)newCapacity;
      }
    }
  }

  private:
  T** mAllThings;
  S mCapacity;
  S mSize;
};

#endif // #ifndef GROWARRAY
