#ifndef CHARBUFFER
#define CHARBUFFER
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

template<class CharType, class IndexType>
class CharBuffer
{
  public:

  CharBuffer()
  {
    mCapacity = 0;
    mSize = 0;
    mChars = NULL;
  }
  
  ~CharBuffer()
  {
    if(mChars != NULL)
    {
      free(mChars);
    }
    mCapacity = 0;
    mSize = 0;
  }

  void SetCapacity(IndexType newCapacity)
  {
    mSize = 0;
    if(newCapacity>=1)
    {
      if(mCapacity != newCapacity)
      {
        if(mChars!=NULL)
        {
          free(mChars);
        }
        mChars = (CharType*)calloc(newCapacity, sizeof(CharType));
        mCapacity = newCapacity;
      }
    }
  }
  
  CharType operator[](IndexType index)
  {
    if((index >= 0) && (index < mSize) && (mChars!=NULL))
    {
      return mChars[index];  
    }
    return 0;
  }

  void Set( IndexType index, CharType ch )
  {
    if((mCapacity>0) && (index < mCapacity))
    {
      mChars[index] = ch;
      if(index+1>mSize)
      {
        mSize = index+1;
      }
    }
  }
  
  IndexType Capacity()
  {
    return mCapacity;
  }
  
  IndexType Size()
  {
    return mSize;
  }

  CharType* GetChars()
  {
    if(mChars!=NULL)
    {
      mChars[mSize] = (CharType)'\0'; // NJM - terminate the string before returning a pointer
    }
    return mChars;
  }

  bool Append(const CharType* src)
  {
    if(mChars!=NULL)
    {
      IndexType indexLimit = mCapacity-1;
      CharType* dst = mChars+mSize;
      while((mSize<indexLimit) && (*src!=0))
      {
        *dst++ = *src++;
        ++mSize;
      }
      return (*src==0);
    }
    return false;
  }

  bool Append(int num)
  {
    CharType charBuffer[22];
    sprintf(charBuffer, "%d", num);
    return Append(charBuffer);
  }
  
  void Clear()
  {
    mSize = 0;
  }
  
  private:
  CharType* mChars;
  IndexType mCapacity;
  IndexType mSize;
};

#endif // #ifndef CHARBUFFER
