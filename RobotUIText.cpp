#include "RobotUIText.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif
  
RobotUIText::RobotUIText(TextSize textSize)
{
  mTextSize = textSize;
}

void RobotUIText::SetText(const char* newText)
{
  if(newText != NULL)
  {
    RobotUI::CommandStart();
    RobotUI::CommandAppend(APP_CMD_SET_TEXT);
    RobotUI::CommandAppend(GetWidgetIndexAsInt());
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(newText);
    RobotUI::CommandAppend("}");
    RobotUI::CommandEnd();
  }
}

void RobotUIText::AppendText(const char* newText)
{
  if(newText != NULL)
  {
    RobotUI::CommandStart();
    RobotUI::CommandAppend(APP_CMD_APPEND_TEXT);
    RobotUI::CommandAppend(GetWidgetIndexAsInt());
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(newText);
    RobotUI::CommandAppend("}");
    RobotUI::CommandEnd();
  }
}

