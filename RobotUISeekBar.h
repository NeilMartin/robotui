#ifndef ROBOTUISEEKBAR
#define ROBOTUISEEKBAR
#include "YourWidget.h"

class RobotUISeekBar : public RobotUIWidget
{
  public:

  RobotUISeekBar(int maxValue);

  int GetProgress();
  int GetMax();
  void SetProgressEnabled(int progress, bool enabled);
  void SetProgress(int progress);
  void ForceProgress(int progress);
  void ConsumeHasChanged();
  bool HasChanged();

  virtual char GetCharCode()
  {
    return CODE_SEEK_BAR;
  }
  void SetProgressFromApp(int progress);

  private:
  void SetProgressInternal(int progress, bool commandFromApp, bool forceSend);
  void SetProgressEnabledInternal(int progress, bool enabled, bool commandFromApp, bool forceSend);
  void SendState();
  
  int mProgress;
  int mMaxValue;
  bool mEnabled;
  bool mHasChanged;
};

#endif // #ifndef ROBOTUISEEKBAR
