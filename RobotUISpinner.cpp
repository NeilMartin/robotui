#include "RobotUISpinner.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

RobotUISpinner::RobotUISpinner()
{
  mHasChanged = false;
  mEnabled = true;
  mSelectedOption = 0;
}

void RobotUISpinner::SetOptions(char const * const * const options, char numOptions, char selectedOption)
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_NAME_OPTIONS);
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend((int)numOptions);
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend((int)selectedOption);
  for(int i=0;i<numOptions;++i)
  {
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(options[i]);
    RobotUI::CommandAppend("}");
  }
  RobotUI::CommandEnd();

  mSelectedOption = selectedOption;
}

void RobotUISpinner::SetOptionsF(char const * const * const options PROGMEM, char numOptions, char selectedOption)
{
  const int bufferSize = 64;
  char charBuffer[bufferSize];
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_NAME_OPTIONS);
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend((int)numOptions);
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend((int)selectedOption);
  for(int i=0;i<numOptions;++i)
  {
    RobotUI::CommandAppend("{");
    strncpy_P(charBuffer, (char*)pgm_read_word(&(options[i])), bufferSize-1);
    charBuffer[bufferSize-1] = 0;
    RobotUI::CommandAppend(charBuffer);
    RobotUI::CommandAppend("}");
  }
  RobotUI::CommandEnd();
  mSelectedOption = selectedOption;
}

void RobotUISpinner::SetOption(char optionIndex)
{
  SetOptionInternal(optionIndex, false, false);
}

void RobotUISpinner::SetOptionFromApp(char optionIndex)
{
  SetOptionInternal(optionIndex, true, false);
}

void RobotUISpinner::SetOptionInternal(char optionIndex, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if(mSelectedOption != optionIndex)
  {
    mSelectedOption = optionIndex;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUISpinner::SetOptionEnabled(char optionIndex, bool enabled)
{
  SetOptionEnabledInternal(optionIndex, enabled, false, false);
}

void RobotUISpinner::SetOptionEnabledFromApp(char optionIndex, bool enabled)
{
  SetOptionEnabledInternal(optionIndex, enabled, true, false);
}

void RobotUISpinner::SetOptionEnabledInternal(char optionIndex, bool enabled, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if((mSelectedOption != optionIndex) || (mEnabled != enabled))
  {
    mSelectedOption = optionIndex;
    mEnabled = enabled;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUISpinner::SendState()
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_SPINNER);
  RobotUI::CommandAppend((int)mSelectedOption);
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend(mEnabled?"1":"0");
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandEnd();
}

void RobotUISpinner::ConsumeHasChanged()
{
  mHasChanged = false;
}

bool RobotUISpinner::HasChanged()
{
  return mHasChanged;
}

char RobotUISpinner::GetOption()
{
  return mSelectedOption;
}

