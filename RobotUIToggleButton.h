#ifndef ROBOTUITOGGLEBUTTON
#define ROBOTUITOGGLEBUTTON
#include "YourWidget.h"

class RobotUIToggleButton : public RobotUIWidget
{
  public:

  RobotUIToggleButton(TextSize textSize = DEFAULT_TEXT_SIZE);

  virtual char GetCharCode()
  {
    return CODE_TOGGLE_BUTTON;
  }

  virtual void SetNames(const char* newOnName, const char* newOffName);
  virtual void SetNames(const __FlashStringHelper* newOnName, const __FlashStringHelper* newOffName);
  bool IsPressed();
  void SetIsPressedEnabled(bool pressed, bool enabled);
  void SetIsPressed(bool pressed);
  void ForceIsPressed(bool pressed);
  void ConsumeHasChanged();
  bool HasChanged();
  
  virtual TextSize GetTextSize()
  {
    return mTextSize;
  }
  void SetIsPressedFromApp(bool pressed);

  private:
  void SetIsPressedInternal(bool pressed, bool commandFromApp, bool forceSend);
  void SetIsPressedEnabledInternal(bool pressed, bool enabled, bool commandFromApp, bool forceSend);
  void SendState();
  
  bool mIsPressed;
  bool mEnabled;
  bool mHasChanged;
  TextSize mTextSize;
};

#endif // #ifndef ROBOTUITOGGLEBUTTON
