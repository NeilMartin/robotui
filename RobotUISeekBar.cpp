#include "RobotUISeekBar.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

RobotUISeekBar::RobotUISeekBar(int maxValue)
{
  mHasChanged = false;
  mEnabled = true;
  mProgress = 0;
  mMaxValue = maxValue;
}

int RobotUISeekBar::GetProgress()
{
  return mProgress;
}

int RobotUISeekBar::GetMax()
{
  return mMaxValue;
}

void RobotUISeekBar::SetProgress(int progress)
{
  SetProgressInternal(progress, false, false);
}

void RobotUISeekBar::ForceProgress(int progress)
{
  SetProgressInternal(progress, false, true);
}

void RobotUISeekBar::SetProgressFromApp(int progress)
{
  SetProgressInternal(progress, true, false);
}

void RobotUISeekBar::SetProgressInternal(int progress, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if(mProgress != progress)
  {
    mProgress = progress;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUISeekBar::SetProgressEnabled(int progress, bool enabled)
{
  SetProgressEnabledInternal(progress, enabled, false, false);
}

void RobotUISeekBar::SetProgressEnabledInternal(int progress, bool enabled, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if((mProgress != progress) || (mEnabled != enabled))
  {
    mProgress = progress;
    mEnabled = enabled;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUISeekBar::SendState()
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_SEEK_BAR);
  RobotUI::CommandAppend((int)mProgress);
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend((int)mMaxValue);
  RobotUI::CommandAppend("_");
  RobotUI::CommandAppend(mEnabled?"1":"0");
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandEnd();
}

void RobotUISeekBar::ConsumeHasChanged()
{
  mHasChanged = false;
}

bool RobotUISeekBar::HasChanged()
{
  return mHasChanged;
}

