#ifndef ROBOTUISWITCH
#define ROBOTUISWITCH
#include "YourWidget.h"

class RobotUISwitch : public RobotUIWidget
{
  public:
  
  RobotUISwitch(TextSize textSize = DEFAULT_TEXT_SIZE);
  
  virtual TextSize GetTextSize()
  {
    return mTextSize;
  }

  virtual void SetName(const char* newName);
  virtual void SetName(const __FlashStringHelper* newName);
  bool IsPressed();
  void SetIsPressedEnabled(bool pressed, bool enabled);
  void SetIsPressed(bool pressed);
  void ForceIsPressed(bool pressed);
  void ConsumeHasChanged();
  bool HasChanged();

  virtual char GetCharCode()
  {
    return CODE_SWITCH;
  }
  void SetIsPressedFromApp(bool pressed);

  private:
  void SetIsPressedInternal(bool pressed, bool commandFromApp, bool forceSend);
  void SetIsPressedEnabledInternal(bool pressed, bool enabled, bool commandFromApp, bool forceSend);
  void SendState();

  TextSize mTextSize;
  bool mIsPressed;
  bool mEnabled;
  bool mHasChanged;
};

#endif // #ifndef ROBOTUISWITCH
