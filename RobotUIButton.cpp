#include "RobotUIButton.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

RobotUIButton::RobotUIButton()
{
  mIsPressedCount = 0;
  mEnabled = true;
}

void RobotUIButton::SetName(const char* newName)
{
  if(newName != NULL)
  {
    RobotUI::CommandStart();
    RobotUI::CommandAppend(APP_CMD_SET_NAME);
    RobotUI::CommandAppend(GetWidgetIndexAsInt());
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(newName);
    RobotUI::CommandAppend("}");
    RobotUI::CommandEnd();
  }
}

void RobotUIButton::SetName(const __FlashStringHelper* newName)
{
  if(newName != NULL)
  {
    const int bufferSize = 64;
    char charBuffer[bufferSize];
    strncpy_P(charBuffer, (const char*)newName, bufferSize-1);
    charBuffer[bufferSize-1] = 0;
    SetName(charBuffer);
  }
}

void RobotUIButton::SetIsEnabled(bool enabled)
{
  SetIsEnabledInternal(enabled, false, false);
}

void RobotUIButton::SetIsEnabledFromApp(bool enabled)
{
  SetIsEnabledInternal(enabled, true, false);
}

void RobotUIButton::SetIsEnabledInternal(bool enabled, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if(mEnabled != enabled)
  {
    mEnabled = enabled;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUIButton::SendState()
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_ENABLED);
  RobotUI::CommandAppend(mEnabled?"1":"0");
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandEnd();
}

bool RobotUIButton::IsPressed()
{
  return mIsPressedCount > 0;
}

void RobotUIButton::ConsumeIsPressed()
{
  if(mIsPressedCount>0)
  {
    --mIsPressedCount;
  }
}

void RobotUIButton::AddIsPressedEvent()
{
  if(mIsPressedCount<254)
  {
    ++mIsPressedCount;
  }
}

