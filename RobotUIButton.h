#ifndef ROBOTUIBUTTON
#define ROBOTUIBUTTON
#include "YourWidget.h"

class RobotUIButton : public RobotUIWidget
{
  public:

  RobotUIButton();//TextSize textSize = DEFAULT_TEXT_SIZE);
  
  virtual void SetName(const char* newName);
  virtual void SetName(const __FlashStringHelper* newName);
  void SetIsEnabled(bool enabled);
  bool IsPressed();
  void ConsumeIsPressed();

  virtual char GetCharCode()
  {
    return CODE_BUTTON;
  }
  void SetIsEnabledFromApp(bool enabled);
  void AddIsPressedEvent();

  private:
  void SetIsEnabledInternal(bool enabled, bool commandFromApp, bool forceSend);
  void SendState();

  private:
  PressCount mIsPressedCount;
  bool mEnabled;
};

class RobotUISpaceWidget : public RobotUIWidget
{
  public:
  RobotUISpaceWidget() : RobotUIWidget(SPACE_WID)
  {
  }
  
  virtual char GetCharCode()
  {
    return CODE_SPACE;
  }
};

class RobotUIGreySpaceWidget : public RobotUIWidget
{
  public:
  RobotUIGreySpaceWidget() : RobotUIWidget(SPACE_WID)
  {
  }
  
  virtual char GetCharCode()
  {
    return CODE_GREY_SPACE;
  }
};


#endif // #ifndef ROBOTUIBUTTON
