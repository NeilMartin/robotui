#ifndef ROBOTUITEXT
#define ROBOTUITEXT
#include "YourWidget.h"

class RobotUIText : public RobotUIWidget
{
  public:

  RobotUIText(TextSize textSize = DEFAULT_TEXT_SIZE);

  virtual char GetCharCode()
  {
    return CODE_TEXT;
  }

  virtual void SetText(const char* newName);
  virtual void AppendText(const char* newName);
  
  virtual TextSize GetTextSize()
  {
    return mTextSize;
  }

  private:
  TextSize mTextSize;
};

#endif // #ifndef ROBOTUITEXT
