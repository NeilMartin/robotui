#include <SoftwareSerial.h>

#define PIN_02_RX       2
#define PIN_03_TX       3

SoftwareSerial*  btSerial(PIN_02_RX, PIN_03_TX); // RX, TX

long int ms = 0;
char* command = "AT+VERSION";
//char* command = "AT";
int baudRateIndex = 0;
int postFixIndex = 3;
int attemptIndex = 0;

//char* commandPostfix = "\n\r"; // NJM - works with 00:21:13:00:DA:EC

#define NUM_RATES  8
long int baudRates[NUM_RATES] = { 9600, 38400, 1200, 2400, 4800, 19200, 57600, 115200 };

#define NUM_POSTFIXS  5
char* commandPostFixs[NUM_POSTFIXS] = { "", "\n", "\r", "\n\r", "\r\n" };

#define NUM_ATTEMPTS  2

void setup() 
{
  Serial.begin(9600);

  btSerial = new SoftwareSerial(PIN_02_RX, PIN_03_TX); // RX, TX
  
  btSerial->begin(baudRates[baudRateIndex]);
  Serial.print("SoftwareSerial baudrate=");
  Serial.println(baudRates[baudRateIndex]);

  ms = millis()+1000;
}

void loop()
{
  if(btSerial->available()>0)
  {
    while(btSerial->available()>0)
    {
      Serial.write(btSerial->read());
    }
  }
  else if (millis()>ms)
  {
    ms = millis()+200;
    Serial.print("sending: '");
    Serial.print(command);
    Serial.print("' postfix[");
    Serial.print(postFixIndex);
    Serial.println("]");
    btSerial->print(command);
    btSerial->print(commandPostFixs[postFixIndex]);

    attemptIndex = (attemptIndex+1)%NUM_ATTEMPTS;
    if(attemptIndex == 0)
    {
      postFixIndex = (postFixIndex+1)%NUM_POSTFIXS;
      if(postFixIndex == 0)
      {
        delete btSerial;
        btSerial = new SoftwareSerial(PIN_02_RX, PIN_03_TX); // RX, TX
        baudRateIndex = (baudRateIndex+1)%NUM_RATES;
        btSerial->begin(baudRates[baudRateIndex]);
        Serial.print("SoftwareSerial baudrate=");
        Serial.println(baudRates[baudRateIndex]);
      }
    }
  }
}