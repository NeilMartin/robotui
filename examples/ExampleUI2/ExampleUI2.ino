#include <RobotUI.h>

#define BT_BAUDRATE   9600

const char option0[] PROGMEM = "License Plate: Austria";
const char option1[] PROGMEM = "License Plate: Monaco";
const char option2[] PROGMEM = "License Plate: Switzerland";
const char option3[] PROGMEM = "License Plate: United Kingdom";
const char * const options[4] PROGMEM = {option0, option1, option2, option3};

RobotUI rui;
RobotUIButton button00;       
RobotUIButton button01;       
RobotUIButton button02;       
RobotUIButton button03;       
RobotUIButton button04;       
RobotUIButton button05;       
RobotUIButton button06;       
RobotUIButton button07;       
RobotUIButton button08;       
RobotUIButton buttonLeft;     
RobotUIButton buttonRight;    
RobotUIButton buttonUp;       
RobotUIButton buttonDown;     
RobotUIButton buttonOn;       
RobotUIButton buttonOff;      
RobotUIButton buttonOilSlick; 
RobotUIButton buttonLasers;   
RobotUIButton buttonEject;    
RobotUISeekBar seekBar(4);    // bar has 5 discreet steps, 0 to 4.
RobotUISpinner spinner;

void LayoutDef()
{
  rui.StartLayout();
  rui.StartHorizontal(3);     // start a horizontal layout with the height of 3 buttons
  rui.Add(buttonLeft, 1, 20); // add a button with weight 1 to the above horizontal layout, text size 20
  rui.StartVertical(3);       // add a vertical layout of weight 3 to the above horizontal layout
  rui.StartHorizontal(1);
  rui.Add(button00, 1, 30);
  rui.Add(button01, 1, 30);
  rui.Add(button02, 1, 30);
  rui.EndHorizontal();
  rui.StartHorizontal(1);
  rui.Add(button03, 1, 30);
  rui.Add(button04, 1, 20);
  rui.Add(button05, 1, 30);
  rui.EndHorizontal();
  rui.StartHorizontal(1);
  rui.Add(button06, 1, 30);
  rui.Add(button07, 1, 30);
  rui.Add(button08, 1, 30);
  rui.EndHorizontal();
  rui.EndVertical();
  rui.Add(buttonRight, 1, 20);
  rui.EndHorizontal();
  rui.Add(buttonUp, 1, 20);
  rui.Add(seekBar, 1, 20);
  rui.Add(buttonDown, 1, 20);
  rui.StartHorizontal(1);
  rui.Add(buttonOn, 1, 20);
  rui.Add(buttonOff, 1, 20);
  rui.EndHorizontal();
  rui.StartHorizontal(1);
  rui.Add(buttonOilSlick, 1, 20);
  rui.Add(buttonLasers, 1, 20);
  rui.Add(buttonEject, 1, 20);
  rui.EndHorizontal();
  rui.Add(spinner, 1, 20);
  rui.EndLayout();

}

// NJM - This function gets called when the RobotUI app requests a UI status refresh on start up
void RecoverStatus()
{
  button00.SetName("\u2196");   // set button character to NW arrow
  button01.SetName("\u2191");   // set button character to N arrow
  button02.SetName("\u2197");   // set button character to NE arrow
  button03.SetName("\u2190");   // set button character to W arrow
  button04.SetName(F("stop"));     // set button text
  button05.SetName("\u2192");   // set button character to E arrow
  button06.SetName("\u2199");   // set button character to SE arrow
  button07.SetName("\u2193");   // set button character to S arrow
  button08.SetName("\u2198");   // set button character to SW arrow
  buttonLeft.SetName(F("Left"));
  buttonRight.SetName(F("Right"));
  buttonUp.SetName(F("Shift Up"));
  buttonDown.SetName(F("Shift Down"));
  buttonOn.SetName(F("Headlights On"));
  buttonOff.SetName(F("Headlights Off"));
  buttonOilSlick.SetName(F("Oil Slick"));
  buttonLasers.SetName(F("Lasers"));
  buttonEject.SetName(F("Eject Seat"));
  spinner.SetOptionsF(options, 4, 0);
  seekBar.SetProgress(5);
}

void setup()
{
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB
  }
  Serial.println("v0.1");
  pinMode(LED_BUILTIN, OUTPUT);
  rui.Init(ROBOTUI_PIN_RX, ROBOTUI_PIN_TX, BT_BAUDRATE, 255, LayoutDef, RecoverStatus);
}

void loop()
{
  // NJM - You need to call the Update() function often, so the RobotUI library can do its work
  rui.Update();
  if(buttonOn.IsPressed())
  {
    buttonOn.ConsumeIsPressed();
    digitalWrite(LED_BUILTIN, HIGH);
  }
  if(buttonOff.IsPressed())
  {
    buttonOff.ConsumeIsPressed();
    digitalWrite(LED_BUILTIN, LOW);
  }
}