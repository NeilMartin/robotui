#include <RobotUI.h>

#define BT_BAUDRATE   9600

RobotUI rui;
RobotUIButton buttonOn;
RobotUIButton buttonOff;

void LayoutDef()
{
  rui.StartLayout();
  rui.Add(buttonOn,  6, 100); // button height of 6 buttons, text size 100
  rui.Add(buttonOff, 6, 100); // button height of 6 buttons, text size 100
  rui.EndLayout();
}

// NJM - This function gets called when the RobotUI app requests a UI status refresh on start up
void RecoverStatus()
{
  buttonOn.SetName("On");   // set button1 name to "On"
  buttonOff.SetName("Off"); // set button2 name to "Off"
}

void setup()
{
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB
  }
  Serial.println("v0.1");
  pinMode(LED_BUILTIN, OUTPUT);
  rui.Init(ROBOTUI_PIN_RX, ROBOTUI_PIN_TX, BT_BAUDRATE, DEFAULT_COMMAND_BUFFER_SIZE, LayoutDef, RecoverStatus);
}

void loop() 
{
  // NJM - You need to call the Update() function often, so the RobotUI library can do its work
  rui.Update(); 
  if(buttonOn.IsPressed())
  {
    buttonOn.ConsumeIsPressed();
    digitalWrite(LED_BUILTIN, HIGH);
  }
  if(buttonOff.IsPressed())
  {
    buttonOff.ConsumeIsPressed();
    digitalWrite(LED_BUILTIN, LOW);
  }
}