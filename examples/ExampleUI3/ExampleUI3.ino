#include <RobotUI.h>

#define BT_BAUDRATE   9600

#define NUM_PLAYERS 6
RobotUI rui;
RobotUIButton buttonHealAll;       
RobotUIButton buttonHPUp[NUM_PLAYERS];       
RobotUIButton buttonHPDown[NUM_PLAYERS];       
RobotUIButton buttonHPMaxUp[NUM_PLAYERS];       
RobotUIButton buttonHPMaxDown[NUM_PLAYERS];    
RobotUIText   textHP[NUM_PLAYERS];
RobotUIText	  textHPMax[NUM_PLAYERS];

int playerHPMax[NUM_PLAYERS];
int playerHP[NUM_PLAYERS];

void LayoutDef()
{
  rui.StartLayout();
  rui.AddLabel("Current Hit Points", 1, 30, JUSTIFY_BOTTOM);
  rui.StartHorizontal(4);     // start a horizontal layout with the height of 3 buttons
  for(int i=0;i<NUM_PLAYERS;++i)
  {
    rui.StartVertical(1);       // add a vertical layout of weight 3 to the above horizontal layout
    rui.Add(buttonHPUp[i],   2, 50);
    rui.Add(textHP[i],       1, 30, JUSTIFY_CENTRE);
    rui.Add(buttonHPDown[i], 2, 50);
    rui.EndVertical();
  }
  rui.EndHorizontal();
  rui.AddLabel("Maximum Hit Points", 1, 30, JUSTIFY_BOTTOM);
  rui.StartHorizontal(4);     // start a horizontal layout with the height of 3 buttons
  for(int i=0;i<NUM_PLAYERS;++i)
  {
    rui.StartVertical(1);       // add a vertical layout of weight 3 to the above horizontal layout
    rui.Add(buttonHPMaxUp[i],   2, 50);
    rui.Add(textHPMax[i],       1, 30, JUSTIFY_CENTRE);
    rui.Add(buttonHPMaxDown[i], 2, 50);
    rui.EndVertical();
  }
  rui.EndHorizontal();
  rui.AddLabel("Functions", 1, 30, JUSTIFY_BOTTOM);
  rui.Add(buttonHealAll, 1, 20);
  rui.EndLayout();
}

void SetHPText( int hp, RobotUIText& uiText )
{
  char hpText[10];
  sprintf(hpText, "%d", hp);
  uiText.SetText(hpText);
}

// NJM - This function gets called when the RobotUI app requests a UI status refresh on start up
void RecoverStatus()
{
  buttonHealAll.SetName("Heal all");
  for(int i=0;i<NUM_PLAYERS;++i)
  {
    buttonHPUp[i].SetName("\u2191");     	// set button character to N arrow
    buttonHPDown[i].SetName("\u2193");   	// set button character to S arrow
    buttonHPMaxUp[i].SetName("\u2191");   // set button character to N arrow
    buttonHPMaxDown[i].SetName("\u2193"); // set button character to S arrow
    SetHPText( playerHP[i], textHP[i] );
    SetHPText( playerHPMax[i], textHPMax[i] );
  }
}

void HandleHPUpButton( RobotUIButton& button, int &hp, int hpMax, RobotUIText& uiText )
{
  if(button.IsPressed())
  {
    button.ConsumeIsPressed();
  	if(hp<hpMax)
  	{
  		SetHPText( ++hp, uiText );
  	}
  }
}

void HandleHPDownButton( RobotUIButton& button, int &hp, RobotUIText& uiText )
{
  if(button.IsPressed())
  {
    button.ConsumeIsPressed();
  	if(hp>0)
  	{
  		SetHPText( --hp, uiText );
  	}
  }
}

void setup()
{
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB
  }
  Serial.println("v0.1");
  rui.Init(ROBOTUI_PIN_RX, ROBOTUI_PIN_TX, BT_BAUDRATE, DEFAULT_COMMAND_BUFFER_SIZE, LayoutDef, RecoverStatus);
  for(int i=0;i<NUM_PLAYERS;++i)
  {
    playerHPMax[i] = 20;
    playerHP[i] = 10;
  }
}

void loop()
{
  // NJM - You need to call the Update() function often, so the RobotUI library can do its work
  rui.Update();
  if(buttonHealAll.IsPressed())
  {
	  buttonHealAll.ConsumeIsPressed();
    for(int i=0;i<NUM_PLAYERS;++i)
    {
	    playerHP[i] = playerHPMax[i];	
	    SetHPText( playerHP[i], textHP[i] );
    }
  }
  for(int i=0;i<NUM_PLAYERS;++i)
  {
    HandleHPUpButton(buttonHPUp[i], playerHP[i], playerHPMax[i], textHP[i] );
    HandleHPDownButton(buttonHPDown[i], playerHP[i], textHP[i] );
    HandleHPUpButton(buttonHPMaxUp[i], playerHPMax[i], 500, textHPMax[i] );
    HandleHPDownButton(buttonHPMaxDown[i], playerHPMax[i], textHPMax[i] );
  }
}
