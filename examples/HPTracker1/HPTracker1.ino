#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <RobotUI.h>

#define BT_BAUDRATE   9600

#define OLED_MOSI   9
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13


#define NUM_PLAYERS 8
#define NUM_CAMPAIGNS 3
#define VALIDATION_CODE (((long int)'N'<<24)|((long int)'J'<<16)|((long int)'M'<<8))
#define FLAG_HIDDEN (1<<0)

namespace HPTracker
{
  extern void setup();
  extern void loop();
  extern void LoadData();
  extern void renderDisplay();
  extern void ClearData();
  extern void FixData();
}

void setup()
{
  HPTracker::setup();
}

void loop()
{
  HPTracker::loop();
}

namespace HPTracker
{
  Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);


  const char option0[] PROGMEM = "Chad's Campaign";
  const char option1[] PROGMEM = "Lando's Campaign";
  const char option2[] PROGMEM = "Other Campaign";
  const char * const campaignOptions[NUM_CAMPAIGNS] PROGMEM = {option0, option1, option2};
  //const char * const campaignOptions[NUM_CAMPAIGNS] PROGMEM = {"Chad's Campaign", "Lando's Campaign", "Other Campaign"};
  #define NUM_ID_OPTIONS 26
  char const * const alphaOptions[NUM_ID_OPTIONS] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
                                                      "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"  };

  RobotUI rui;
  RobotUIButton buttonHealAll;       
  RobotUISpinner selectID[NUM_PLAYERS];       
  RobotUIButton buttonHPUp10[NUM_PLAYERS];       
  RobotUIButton buttonHPUp[NUM_PLAYERS];       
  RobotUIButton buttonHPDown[NUM_PLAYERS];       
  RobotUIButton buttonHPDown10[NUM_PLAYERS];       
  RobotUIButton buttonHPMaxUp[NUM_PLAYERS];       
  RobotUIButton buttonHPMaxDown[NUM_PLAYERS];    
  RobotUIToggleButton checkBox[NUM_PLAYERS];    
  RobotUIButton buttonSwap[NUM_PLAYERS-1];
  RobotUIText   textHP[NUM_PLAYERS];
  RobotUIText   textHPMax[NUM_PLAYERS];
  RobotUIText   textID[NUM_PLAYERS];
  RobotUISpinner selectCampaign;

  class PlayerData
  {
    public:
    int hpMax;
    int hp;
    char id;
    unsigned char flags;
  };

  class SaveData
  {
    public:
    PlayerData playerData[NUM_PLAYERS*NUM_CAMPAIGNS];
    long int validation;
    char campaignIndex;
  };

  SaveData sd;
  int firstPlayerIndex = -1;
  PlayerData *playerData = sd.playerData;
  bool prepareToSave = false;
  unsigned int saveTime = 0;

  void LayoutDef()
  {
    rui.StartLayout();
    rui.AddLabel("Current Hit Points", 1, 30, JUSTIFY_BOTTOM);
    rui.StartHorizontal(8);     // start a horizontal layout with the height of 5 buttons
    for(int i=0;i<NUM_PLAYERS;++i)
    {
      rui.StartVertical(1);       // add a vertical layout of weight 3 to the above horizontal layout
      rui.Add(selectID[i],       1, 10);
      rui.Add(textID[i],         1, 20, JUSTIFY_CENTRE);
      rui.Add(buttonHPUp10[i],   1, 20);
      rui.Add(buttonHPUp[i],     1, 20);
      rui.Add(textHP[i],         1, 20, JUSTIFY_CENTRE);
      rui.Add(buttonHPDown[i],   1, 20);
      rui.Add(buttonHPDown10[i], 1, 20);
      rui.Add(checkBox[i],       1, 10);
      rui.EndVertical();
    }
    rui.EndHorizontal();
    rui.AddLabel("Maximum Hit Points", 1, 30, JUSTIFY_BOTTOM);
    rui.StartHorizontal(3);     // start a horizontal layout with the height of 3 buttons
    for(int i=0;i<NUM_PLAYERS;++i)
    {
      rui.StartVertical(1);       // add a vertical layout of weight 3 to the above horizontal layout
      rui.Add(buttonHPMaxUp[i],   1, 20);
      rui.Add(textHPMax[i],       1, 20, JUSTIFY_CENTRE);
      rui.Add(buttonHPMaxDown[i], 1, 20);
      rui.EndVertical();
    }
    rui.EndHorizontal();
    rui.StartHorizontal(1);     // start a horizontal layout with the height of 3 buttons
    rui.AddSpace(1);
    for(int i=0;i<NUM_PLAYERS-1;++i)
    {
      rui.Add(buttonSwap[i],   2, 20);
    }
    rui.AddSpace(1);
    rui.EndHorizontal();
    rui.AddLabel("Functions", 1, 30, JUSTIFY_BOTTOM);
    rui.Add(buttonHealAll, 1, 20);
    rui.Add(selectCampaign, 1);
    rui.EndLayout();
  }

  void SetHPText( int hp, RobotUIText& uiText )
  {
    char hpText[10];
    sprintf(hpText, "%d", hp);
    uiText.SetText(hpText);
  }

  void RecoverStatusForCampaignSwitch()
  {
    char charToStr[2];
    charToStr[1]=0;
    for(int i=0;i<NUM_PLAYERS;++i)
    {
      PlayerData *pd = playerData + firstPlayerIndex + i;
      SetHPText( pd->hp, textHP[i] );
      SetHPText( pd->hpMax, textHPMax[i] );
      bool visible = (pd->flags & FLAG_HIDDEN)==0;
      buttonHPDown[i].SetIsEnabled(visible);
      buttonHPDown10[i].SetIsEnabled(visible);
      checkBox[i].SetIsPressed(visible);
      //selectID[i].SetOption( (int)(pd->id - 'A'), false, false );
      charToStr[0] = pd->id;
      textID[i].SetText(charToStr);
    }
  }

  // NJM - This function gets called when the RobotUI app requests a UI status refresh on start up
  void RecoverStatus()
  {
    buttonHealAll.SetName("Heal all");
    for(int i=0;i<NUM_PLAYERS;++i)
    {
      buttonHPUp10[i].SetName("+10");   
      buttonHPUp[i].SetName("+1");      
      buttonHPDown[i].SetName("-1");    
      buttonHPDown10[i].SetName("-10"); 
      buttonHPMaxUp[i].SetName("+1");   
      buttonHPMaxDown[i].SetName("-1"); 
      checkBox[i].SetNames("hide", "show");
      checkBox[i].SetIsPressedEnabled( false, true );
      selectID[i].SetOptions(alphaOptions, NUM_ID_OPTIONS, 0);
    }
    for(int i=0;i<NUM_PLAYERS-1;++i)
    {
      buttonSwap[i].SetName("<->");
    }
    selectCampaign.SetOptionsF(campaignOptions, NUM_CAMPAIGNS, sd.campaignIndex);
    RecoverStatusForCampaignSwitch();
  }

  bool HandleHPUpButton( RobotUIButton& button, int &hp, int hpMax, RobotUIText& uiText, int amount )
  {
    if(button.IsPressed())
    {
      button.ConsumeIsPressed();
      if(hp<hpMax)
      {
        hp += amount;
        if(hp>hpMax)
        {
          hp = hpMax;
        }
        SetHPText( hp, uiText );
        return true;
      }
    }
    return false;
  }

  bool HandleHPDownButton( RobotUIButton& button, int &hp, RobotUIText& uiText, int amount )
  {
    if(button.IsPressed())
    {
      button.ConsumeIsPressed();
      if(hp>0)
      {
        hp -= amount;
        if(hp<0)
        {
          hp = 0;
        }
        SetHPText( hp, uiText );
        return true;
      }
    }
    return false;
  }

  void setup()
  {
    Serial.begin(9600);
    display.begin(SSD1306_SWITCHCAPVCC);
    display.display();
    
    while (!Serial)
    {
      ; // wait for serial port to connect. Needed for native USB
    }
    Serial.println("v0.1");
    pinMode(LED_BUILTIN, OUTPUT);
    rui.Init(ROBOTUI_PIN_RX, ROBOTUI_PIN_TX, BT_BAUDRATE, DEFAULT_COMMAND_BUFFER_SIZE*2, LayoutDef, RecoverStatus);
    HPTracker::LoadData();
    renderDisplay();
  }

  void LoadData()
  {
    int eeAddress = 0;
    EEPROM.get( eeAddress, sd );
    if(sd.validation != VALIDATION_CODE)
    {
      ClearData();
    }
    else
    {
      FixData();
    }
    firstPlayerIndex = sd.campaignIndex*NUM_PLAYERS;
  }

  void SaveData()
  {
    if(sd.validation == VALIDATION_CODE)
    {
      int eeAddress = 0;
      EEPROM.put( eeAddress, sd );
    }
  }

  void ClearData()
  {
    int numPlayers = NUM_PLAYERS * NUM_CAMPAIGNS;
    for(int i=0;i<numPlayers;++i)
    {
      int ii = (i%NUM_PLAYERS);
      PlayerData *pd = playerData + i;
      pd->hpMax = 50+ii*5;
      pd->hp = 45+ii*5;
      pd->id = 'a' + i%26;
      pd->flags = 0;
    }
    sd.validation = VALIDATION_CODE;
    sd.campaignIndex = 0;
    firstPlayerIndex = sd.campaignIndex*NUM_PLAYERS;
  }

  void FixData()
  {
    int numPlayers = NUM_PLAYERS * NUM_CAMPAIGNS;
    for(int i=0;i<numPlayers;++i)
    {
      int ii = (i%NUM_PLAYERS);
      PlayerData *pd = playerData + i;
      if((pd->id<'A') || (pd->id>'Z'))
      {
        pd->id = 'A' + (pd->id-'a')%26;
      }
    }
    sd.validation = VALIDATION_CODE;
    if((sd.campaignIndex<0) || (sd.campaignIndex>=NUM_CAMPAIGNS))
    {
      sd.campaignIndex = 0;
    }
    firstPlayerIndex = sd.campaignIndex*NUM_PLAYERS;
  }

  void renderDisplay()
  {
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    int x3 = 1+4;
    int xBorder = 20;
    int x2 = display.width()-(2*xBorder)-x3;
    PlayerData *pd = playerData + firstPlayerIndex;
    int xHighestMax = pd->hpMax;
    bool highestFound = ((pd->flags & FLAG_HIDDEN)==0);
    for(int i=1;i<NUM_PLAYERS;++i)
    {
      pd = playerData + firstPlayerIndex + i;
      if(((pd->flags & FLAG_HIDDEN)==0) && ((!highestFound) || (pd->hpMax > xHighestMax)))
      {
        xHighestMax = pd->hpMax;
        highestFound = true;
      }
    }
    for(int i=0;i<NUM_PLAYERS;++i)
    {
      pd = playerData + firstPlayerIndex + i;
      if((pd->flags & FLAG_HIDDEN)==0)
      {
        display.setCursor(0,i*8);
        display.print(pd->id);
        if(pd->hp<100)
        {
          display.print(" ");
        }
        if(pd->hp<10)
        {
          display.print(" ");
        }
        display.print(pd->hp);
        int xCurr = ((x2-2)*pd->hp)/xHighestMax;
        int xMax = (x2*pd->hpMax)/xHighestMax;
        display.fillRect(xBorder+x3, i*8+1, xMax, 6, WHITE);
        display.fillRect(xBorder+x3+xCurr+1, i*8+2, xMax-xCurr-2, 4, BLACK);
        display.setCursor(xBorder+x3+xMax+2,i*8);
        display.print(pd->hpMax);
      }
    }
    display.display();
  }

  void loop()
  {
    bool changed = false;
    // NJM - You need to call the Update() function often, so the RobotUI library can do its work
    unsigned long ms = rui.Update();
    if(buttonHealAll.IsPressed())
    {
      buttonHealAll.ConsumeIsPressed();
      for(int i=0;i<NUM_PLAYERS;++i)
      {
        PlayerData *pd = playerData + firstPlayerIndex + i;
        pd->hp = pd->hpMax; 
        SetHPText( pd->hp, textHP[i] );
      }
      changed = true;
    }
    if(selectCampaign.HasChanged())
    {
      selectCampaign.ConsumeHasChanged();
      if(selectCampaign.GetOption() != sd.campaignIndex)
      {
        // NJM - Check if we are about to save the current campaign, if so, save before we switch
        if(prepareToSave)
        {
          SaveData();
          prepareToSave = false;
        }
        sd.campaignIndex = selectCampaign.GetOption();
        firstPlayerIndex = (int)sd.campaignIndex * NUM_PLAYERS;
        changed = true;
        RecoverStatusForCampaignSwitch();
      }
    }
    char charToStr[2];
    charToStr[1] = 0;
    for(int i=0;i<NUM_PLAYERS;++i)
    {
      PlayerData *pd = playerData + firstPlayerIndex + i;
      changed |= HandleHPUpButton(buttonHPUp[i], pd->hp, pd->hpMax, textHP[i], 1 );
      changed |= HandleHPDownButton(buttonHPDown[i], pd->hp, textHP[i], 1 );
      changed |= HandleHPUpButton(buttonHPUp10[i], pd->hp, pd->hpMax, textHP[i], 10 );
      changed |= HandleHPDownButton(buttonHPDown10[i], pd->hp, textHP[i], 10 );
      changed |= HandleHPUpButton(buttonHPMaxUp[i], pd->hpMax, 500, textHPMax[i], 1 );
      changed |= HandleHPDownButton(buttonHPMaxDown[i], pd->hpMax, textHPMax[i], 1 );
      if(checkBox[i].HasChanged())
      {
        checkBox[i].ConsumeHasChanged();
        bool visible = checkBox[i].IsPressed();
        if(visible)
        {
          pd->flags &= ~FLAG_HIDDEN;
        }
        else
        {
          pd->flags |= FLAG_HIDDEN;
        }
        changed = true;
        buttonHPDown[i].SetIsEnabled(visible);
        buttonHPDown10[i].SetIsEnabled(visible);
      }
      if(selectID[i].HasChanged())
      {
        selectID[i].ConsumeHasChanged();
        pd->id = 'A' + selectID[i].GetOption();
        changed = true;
        charToStr[0] = pd->id;
        textID[i].SetText(charToStr);
      }
    }
    for(int i=0;i<NUM_PLAYERS-1;++i)
    {
      if(buttonSwap[i].IsPressed())
      {
        buttonSwap[i].ConsumeIsPressed();
        PlayerData *pd0 = playerData + firstPlayerIndex + i;
        PlayerData *pd1 = pd0+1;
        PlayerData temp = *pd0;
        *pd0 = *pd1;
        *pd1 = temp;
        changed = true;
        SetHPText( pd0->hp,    textHP[i] );
        SetHPText( pd0->hpMax, textHPMax[i] );
        SetHPText( pd1->hp,    textHP[i+1] );
        SetHPText( pd1->hpMax, textHPMax[i+1] );
        bool visible0 = (pd0->flags & FLAG_HIDDEN)==0;
        bool visible1 = (pd1->flags & FLAG_HIDDEN)==0;
        checkBox[i].SetIsPressed(visible0);
        checkBox[i+1].SetIsPressed(visible1);
        buttonHPDown[i].SetIsEnabled(visible0);
        buttonHPDown[i+1].SetIsEnabled(visible1);
        buttonHPDown10[i].SetIsEnabled(visible0);
        buttonHPDown10[i+1].SetIsEnabled(visible1);
        selectID[i].SetOption( (int)(pd0->id-'A') );
        selectID[i+1].SetOption( (int)(pd1->id-'A') );
      }
    }
    if(changed)
    {
      prepareToSave = true;
      saveTime = ms + 3000;
      renderDisplay();
    }
    if((prepareToSave) && (ms>=saveTime))
    {
      SaveData();
      prepareToSave = false;
    }
  }

}