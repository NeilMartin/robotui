#ifndef ROBOTUI
#define ROBOTUI
#include "CharBuffer.h"
#include "RobotUIButton.h"
#include "RobotUICheckBox.h"
#include "RobotUISeekBar.h"
#include "RobotUISpinner.h"
#include "RobotUISwitch.h"
#include "RobotUIText.h"
#include "RobotUIToggleButton.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

class SoftwareSerial;
class WidgetCont;

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1284__) || defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega644__) || defined(__AVR_ATmega644A__) || defined(__AVR_ATmega644P__) || defined(__AVR_ATmega644PA__)
#define MEGA_2560 1
#endif

#define MAX_COMMAND_SIZE    64
#define DEFAULT_COMMAND_BUFFER_SIZE 127
#define MAX_CONTAINER_DEPTH 4

#if defined(MEGA_2560)
#define ROBOTUI_PIN_RX       50
#define ROBOTUI_PIN_TX       49
#else
#define ROBOTUI_PIN_RX       2
#define ROBOTUI_PIN_TX       3
#endif

#define LSS_INVALID                         0
#define LSS_START                           1
#define LSS_CONT_WAITING_FOR_FIRST_CHILD    2
#define LSS_NODE_COMPLETE                   3

#define DEBUG_OUTPUT
#ifdef DEBUG_OUTPUT
#define DEBUG_PRINTLN_STRING_CONST(_a)  Serial.println(F(_a))
#define DEBUG_PRINT_STRING_CONST(_a)    Serial.print(F(_a))
#define DEBUG_PRINTLN_VAR(_a)           Serial.println((_a))
#define DEBUG_PRINT_VAR(_a)             Serial.print((_a))
#else
#define DEBUG_PRINTLN_STRING_CONST(_a)  
#define DEBUG_PRINT_STRING_CONST(_a)    
#define DEBUG_PRINTLN_VAR(_a)           
#define DEBUG_PRINT_VAR(_a)             
#endif

typedef void (*RecoverFunc)();
typedef void (*LayoutDefFunc)();

class RobotUICheckBox;
class RobotUIToggleButton;

class RobotUI
{
  public:

  RobotUI();
  ~RobotUI();

  static bool IsInitialized();
  static void CommandStart();
  static void CommandAppend(const char* cmd);
  static void CommandAppend(int cmd);
  static void CommandEnd();
  
  void Init(unsigned char pin_rx, unsigned char pin_tx, long int baudRate, int commandBufferSize, LayoutDefFunc layoutDef, RecoverFunc recoverFunc);
  unsigned long Update();
  void StartLayout();
  void EndLayout();
  void StartVertical(float weight);
  void StartVertical(float weight, RobotUICheckBox& toggleWidget, bool visWhenTrue);
  void StartVertical(float weight, RobotUIToggleButton& toggleWidget, bool visWhenTrue);
  void EndVertical();
  void StartHorizontal(float weight);
  void StartHorizontal(float weight, RobotUICheckBox& toggleWidget, bool visWhenTrue);
  void StartHorizontal(float weight, RobotUIToggleButton& toggleWidget, bool visWhenTrue);
  void EndHorizontal();
  void StartVerticalScroll(float weight);
  void EndVerticalScroll();
  void StartHorizontalScroll(float weight);
  void EndHorizontalScroll();
  void Add(RobotUIWidget& yb, float weight = 1.0f, TextSize textSize = DEFAULT_TEXT_SIZE, int justification = DEFAULT_JUSTIFY);
  void AddSpace(float weight);
  void AddGreySpace(float weight);
  void AddLabel(const char* text, float weight = 1.0f, TextSize textSize = DEFAULT_TEXT_SIZE, int justification = DEFAULT_JUSTIFY);
  
  int  GetStreamState();
  void SetStreamState(int newState);
  
  private:
  void StartContainer(int widAsInt, bool vertical, bool scrollable, float weight, int toggleWidgetIndex, bool visWhenTrue);
  void EndContainer(bool vertical, bool scrollable);
  void setupBluetooth(long int baudRate);
  void loopBluetooth();
  void handleCommands(char* const commands);
  void BluetoothSend(const char* msg);
  void CommandBufferStartCMD();
  void CommandBufferAppend(const char* cmd);
  void CommandBufferAppend(int cmd);
  void CommandBufferEndCMD();
  void SendLayout();
  void SendPing();
  void FlushAppCommands();
  void AddInternal(RobotUIWidget& yb, float weight, TextSize textSize, const char* initialText, int justification);

  
  private:
  static bool         mInitialized;
  static RobotUI*     mInstance;

  CharBuffer<char, unsigned char>    mCommandBuffer;
  RobotUISpaceWidget      mSpaceWidget;
  RobotUIGreySpaceWidget  mGreySpaceWidget;
  bool                mCurrentContVertical[MAX_CONTAINER_DEPTH];
  bool                mCurrentContScrollable[MAX_CONTAINER_DEPTH];
  Stream*             mLayoutStream;
  RecoverFunc         mRecoverStatusFunc;
  LayoutDefFunc       mLayoutDefFunc;
  SoftwareSerial*     mBluetooth;
  unsigned long       mLayoutResendMS;
  unsigned long       mExecOnMS;
  unsigned long       mLastMS;
  unsigned long       mLayoutFuncDurationMS;
  int                 mCyclesPer100;
  int                 mCycleCounter;
  int                 mLayoutVersion;
  int                 mCurrentContDepth;
  bool                mLayoutDefMode;
  bool                mLayoutReceived;
  bool                mPartialCommandInProgress;
  unsigned char       mLayoutStreamState;
};

#endif // #ifndef ROBOTUI
