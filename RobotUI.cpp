#include "RobotUI.h"
#include <SoftwareSerial.h>

bool RobotUI::mInitialized = false;
RobotUI* RobotUI::mInstance = NULL;

#define ARDUINO_CMD_REQUEST_LAYOUT  "RequestLayout"
#define ARDUINO_CMD_RECEIVED_LAYOUT "ReceivedLayout"
#define ARDUINO_CMD_BUTTON_PRESSED    "B"
#define ARDUINO_CMD_BUTTON_TOGGLED    "T"
#define ARDUINO_CMD_CHECK_TOGGLED     "C"
#define ARDUINO_CMD_SWITCH_TOGGLED    "S"
#define ARDUINO_CMD_SEEK_BAR_CHANGED  "E"
#define ARDUINO_CMD_SPINNER_CHANGED   "P"
#define APP_CMD_PREFIX                "NJM{"
#define APP_CMD_POSTFIX               "}"
#define APP_CMD_PREFIX_LENGTH         (4)
#define APP_CMD_POSTFIX_LENGTH        (1)
#define APP_CMD_LAYOUT_DEF            "Layout="
#define APP_CMD_PING1_VERSION         "ping1v="
#define APP_CMD_PING2_VERSION         "ping2v="
#define ADDITIONAL_LAYOUT_RESEND_DELAY 1000

#define SINGLE_COMMAND_SIZE           64

RobotUI::RobotUI()
{
  mInstance = this;
  mBluetooth = NULL; 
  mLayoutStream = NULL;
  mLayoutDefMode = false;
  mLayoutReceived = true;
  mLayoutVersion = -1;
  mLastMS = mExecOnMS = millis();
  mCyclesPer100=0;
  mCycleCounter=0;
}

RobotUI::~RobotUI()
{
  mBluetooth = NULL; 
  mLayoutStream = NULL;
  mLayoutDefMode = false;
  mLayoutReceived = true;
  mInstance = NULL;
  mLayoutVersion = -1;
}

bool RobotUI::IsInitialized()
{
  return mInitialized;
}

void RobotUI::Init(unsigned char pin_rx, unsigned char pin_tx, long int baudRate, int commandBufferSize, LayoutDefFunc layoutDefFunc, RecoverFunc recoverFunc)
{
  if(!digitalPinToPCICR(pin_rx))
  {
    Serial.print(F("ERROR: pin "));
    Serial.print(pin_rx);
    Serial.println(F(" can not be used as an RX pin as it is not PCINT"));
    return;
  }
  mBluetooth = new SoftwareSerial(pin_rx, pin_tx);
  mCommandBuffer.SetCapacity(commandBufferSize);
  setupBluetooth(baudRate);
  mInitialized = true;
  mRecoverStatusFunc = recoverFunc;
  mLayoutDefFunc = layoutDefFunc;
  SendLayout();
  //mLayoutResendMS = millis()+ADDITIONAL_LAYOUT_RESEND_DELAY;
  mLayoutResendMS = 0;
  mLayoutFuncDurationMS = 1000;
  mPartialCommandInProgress = false;
}

unsigned long RobotUI::Update()
{
  loopBluetooth();
  return mLastMS;
}

void RobotUI::setupBluetooth(long int baudRate)
{
  long int blueToothBaudRate = baudRate;
  mBluetooth->begin(blueToothBaudRate);
}

void RobotUI::loopBluetooth()
{
  if(++mCycleCounter>=mCyclesPer100)
  {
    mLastMS = millis();
  }
  if(mLastMS>=mExecOnMS)
  {
    if(mCycleCounter>mCyclesPer100)
    {
      mCyclesPer100 = mCycleCounter;
    }
    mCyclesPer100 -= 1;
    mCycleCounter = 0;
    mExecOnMS = mLastMS + 100;
    if(mExecOnMS > (0xffffffff-150))
    {
      mExecOnMS = 0;
    }
    if(!mInitialized)
    {
      mExecOnMS = mLastMS + 5000;
      if(mExecOnMS > (0xffffffff-7000))
      {
        mExecOnMS = 0;
      }
      Serial.println("RobotUI not initialized");
      return;
    }
    int num = mBluetooth->available();
    if(num>0)
    {
      DEBUG_PRINT_STRING_CONST("num bytes received=");
      DEBUG_PRINTLN_VAR(num);
      if((mLayoutReceived == false)&&(mLayoutResendMS==0))
      {
        mLayoutResendMS = mLastMS + ADDITIONAL_LAYOUT_RESEND_DELAY + mLayoutFuncDurationMS;
        DEBUG_PRINT_STRING_CONST("(1) layout will be resent at ");
        DEBUG_PRINTLN_VAR(mLayoutResendMS);
        if(mLayoutResendMS==0)
        {
          mLayoutResendMS = 1;
        }
      }
      if(num < MAX_COMMAND_SIZE-1)
      {
        char charArray[MAX_COMMAND_SIZE];
        int charIndex = 0;
        while(mBluetooth->available() > 0) 
        {
          int incomingByte = mBluetooth->read();
          char c = (char)incomingByte;
          charArray[charIndex++] = c;
        }
        charArray[charIndex++] = '\0';
        DEBUG_PRINTLN_VAR(charArray);
        handleCommands(charArray);
      }
      else
      {
        DEBUG_PRINT_VAR(num);
        DEBUG_PRINT_STRING_CONST(" bytes received, that is larger than the buffer of ");
        DEBUG_PRINTLN_VAR(MAX_COMMAND_SIZE);

        char charArray[MAX_COMMAND_SIZE];
        int charIndex = 0;
        int braceCount = 0;
        bool bracesFound = false;
        bool partialExtracted = true;
        while(mBluetooth->available() > 0) 
        {
          int incomingByte = mBluetooth->read();
          char c = (char)incomingByte;
          charArray[charIndex++] = c;
          if(c=='{')
          {
            ++braceCount;
            bracesFound = true;
          }
          else if (c=='}')
          {
            if((--braceCount==0) && (bracesFound))
            {
              break;
            }
          }
          if(charIndex>=MAX_COMMAND_SIZE-1)
          {
            if(mBluetooth->available()>0)
            {
              partialExtracted = false;
              break;
            }
          }
        }
        charArray[charIndex++] = '\0';
        DEBUG_PRINTLN_VAR(charArray);
        if(partialExtracted)
        {
          handleCommands(charArray);
        }
        else
        {
          DEBUG_PRINT_STRING_CONST("Failed to recover, commands lost");
          // NJM - flush the remaining and hope to recover
          while(mBluetooth->available() > 0) 
          {
            mBluetooth->read();
          }          
        }
      }
    }
    if(mLayoutReceived == false)
    {
      if((mLayoutResendMS!=0)&&(mLastMS>mLayoutResendMS))
      {
        SendLayout();
        mLayoutResendMS = mLastMS + ADDITIONAL_LAYOUT_RESEND_DELAY + mLayoutFuncDurationMS;
        DEBUG_PRINT_STRING_CONST("(2) layout will be resent at ");
        DEBUG_PRINTLN_VAR(mLayoutResendMS);
        if(mLayoutResendMS==0)
        {
          mLayoutResendMS = 1;
        }
      }
      else
      {
        SendPing();
      }
    }
    else if(mCommandBuffer.Size()>0)
    {
      FlushAppCommands();
    }
  }
}

void RobotUI::SendPing()
{
}

void RobotUI::SendLayout()
{
  if(mLayoutDefFunc != NULL)
  {
    mLayoutFuncDurationMS = millis();
    mLayoutDefFunc();
    mLayoutReceived = false;
    mLayoutFuncDurationMS = millis()-mLayoutFuncDurationMS;
    DEBUG_PRINT_STRING_CONST("Layout sent. duration=");
    DEBUG_PRINTLN_VAR(mLayoutFuncDurationMS);
    if(mLayoutFuncDurationMS>10000)
    {
      mLayoutFuncDurationMS=10000;
    }
  }
  else
  {
    DEBUG_PRINTLN_STRING_CONST("mLayoutDefFunc is NULL. This is not allowed");
  }
}

void RobotUI::FlushAppCommands()
{
  int n = mCommandBuffer.Size();
  if((n>0) && (!mPartialCommandInProgress))
  {
    DEBUG_PRINT_VAR(n);
    DEBUG_PRINT_STRING_CONST(" chars. Flushing commands=");
    mBluetooth->println(mCommandBuffer.GetChars());
    DEBUG_PRINTLN_VAR(mCommandBuffer.GetChars());
    mCommandBuffer.Clear();
  }
}

int GetNextCommand( char* const nextCmd, int nextCmdBufferSize, char* const commandStr )
{
  int numBytes = -1;
  char* startChar = strstr(commandStr, APP_CMD_PREFIX);
  if(startChar!=NULL)
  {
    startChar += APP_CMD_PREFIX_LENGTH;
    char* endChar = startChar-1;
    if(*endChar=='{')
    {
      int braceCount = 1;
      char const * const commandLastChar = commandStr + (strlen(commandStr)-1);
      while((endChar<commandLastChar) && (braceCount>0))
      {
        char c = *(++endChar);
        if(c=='}')
        {
          --braceCount;
        }
        else if (c=='{')
        {
          ++braceCount;
        }
      }
      if(braceCount==0)
      {
        --endChar;
        if(endChar-startChar < nextCmdBufferSize)
        {
          numBytes = endChar-startChar;
          char* dst = nextCmd;
          char* src = startChar;
          while(src<=endChar)
          {
            *dst++ = *src++;
          }
          *dst++ = 0;
          DEBUG_PRINT_STRING_CONST("cmd=");
          DEBUG_PRINTLN_VAR(nextCmd);
          endChar++;
          endChar++;
          if(endChar<commandLastChar)
          {
            char* dst = commandStr;
            char* src = endChar;
            while(src<=commandLastChar)
            {
              *dst++ = *src++;
            }
            *dst++ = *src++; // Copy the terminal zero
            DEBUG_PRINT_STRING_CONST("remaining commands = ");
            DEBUG_PRINTLN_VAR(commandStr);
          }
          else
          {
            commandStr[0] = 0;
          }          
        }
        else
        {
          DEBUG_PRINT_STRING_CONST("ERROR: GetNextCommand provided with too small a command buffer size = ");
          DEBUG_PRINTLN_VAR(nextCmdBufferSize);
        }
      }
      else
      {
        DEBUG_PRINT_STRING_CONST("ERROR: Overran end before pairing braces. cmd=");
        DEBUG_PRINTLN_VAR(commandStr);
      }
    }
    else
    {
      DEBUG_PRINTLN_STRING_CONST("ERROR: Expecting the CMD_PREFIX to end in an '{'");
    }
  }
  else
  {
    if(commandStr[0]!=0)
    {
        DEBUG_PRINT_STRING_CONST("ERROR: Command=\"");
        DEBUG_PRINT_VAR(commandStr);
        DEBUG_PRINTLN_STRING_CONST("\"");
    }
  }
  return numBytes;
}

void RobotUI::handleCommands(char* const commands)
{
  char singleCommand[SINGLE_COMMAND_SIZE];
  int numBytes = GetNextCommand( singleCommand, SINGLE_COMMAND_SIZE, commands );
  while(numBytes>0)
  {
    // if singleCommand starts with ARDUINO_CMD_REQUEST_LAYOUT
    if( singleCommand == strstr( singleCommand, ARDUINO_CMD_REQUEST_LAYOUT ) ) 
    {
      SendLayout();
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_RECEIVED_LAYOUT ) )
    {
      DEBUG_PRINTLN_STRING_CONST("Layout confirmed");
      mLayoutReceived = true;
      if(mRecoverStatusFunc!=NULL)
      {
        mRecoverStatusFunc();
      }
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_BUTTON_PRESSED ) )
    {
      int widgetIndex = strtol(singleCommand+1, NULL, 10);
      RobotUIWidget* w = RobotUIWidget::GetWidget(widgetIndex);
      if(w->GetCharCode()==CODE_BUTTON)
      {
        RobotUIButton* yb = (RobotUIButton*)w;
        yb->AddIsPressedEvent();
      }
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_BUTTON_TOGGLED ) )
    {
      bool on = singleCommand[1]!='0';
      int widgetIndex = strtol(singleCommand+2, NULL, 10);
      RobotUIWidget* w = RobotUIWidget::GetWidget(widgetIndex);
      if(w->GetCharCode()==CODE_TOGGLE_BUTTON)
      {
        RobotUIToggleButton* ytb = (RobotUIToggleButton*)w;
        ytb->SetIsPressedFromApp(on);
      }
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_CHECK_TOGGLED ) )
    {
      bool on = singleCommand[1]!='0';
      int widgetIndex = strtol(singleCommand+2, NULL, 10);
      RobotUIWidget* w = RobotUIWidget::GetWidget(widgetIndex);
      if(w->GetCharCode()==CODE_CHECK_BOX)
      {
        RobotUICheckBox* ycb = (RobotUICheckBox*)w;
        ycb->SetIsPressedFromApp(on);
      }
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_SWITCH_TOGGLED ) )
    {
      bool on = singleCommand[1]!='0';
      int widgetIndex = strtol(singleCommand+2, NULL, 10);
      RobotUIWidget* w = RobotUIWidget::GetWidget(widgetIndex);
      if(w->GetCharCode()==CODE_SWITCH)
      {
        RobotUISwitch* ys = (RobotUISwitch*)w;
        ys->SetIsPressedFromApp(on);
        //Serial.print("Switch CMD " + nextCmd + " on=");
        //Serial.println(on);
      }
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_SEEK_BAR_CHANGED ) )
    {
      char* underscoreChar = NULL;
      int progress = strtol(singleCommand+1, &underscoreChar, 10);
      int widgetIndex = strtol(underscoreChar+1, NULL, 10);
      RobotUIWidget* w = RobotUIWidget::GetWidget(widgetIndex);
      if(w->GetCharCode()==CODE_SEEK_BAR)
      {
        RobotUISeekBar* ysb = (RobotUISeekBar*)w;
        ysb->SetProgressFromApp(progress);
      }
    }
    else if( singleCommand == strstr( singleCommand, ARDUINO_CMD_SPINNER_CHANGED ) )
    {
      char* underscoreChar = NULL;
      int optionIndex = strtol(singleCommand+1, &underscoreChar, 10);
      int widgetIndex = strtol(underscoreChar+1, NULL, 10);
      RobotUIWidget* w = RobotUIWidget::GetWidget(widgetIndex);
      if(w->GetCharCode()==CODE_SPINNER)
      {
        RobotUISpinner* ys = (RobotUISpinner*)w;
        ys->SetOptionFromApp(optionIndex);
      }
    }
    else
    {
      //DEBUG_PRINTLN("Unknown command " + nextCmd);
      Serial.print("Unknown command ");
      Serial.println(singleCommand);
    }
    numBytes = GetNextCommand( singleCommand, SINGLE_COMMAND_SIZE, commands );
  }
}

void RobotUI::BluetoothSend(const char* msg)
{
  mBluetooth->print(msg);
}

void RobotUI::StartLayout()
{
  if(!mLayoutDefMode)
  {
    mLayoutDefMode = true;
    mLayoutStream = mBluetooth;
    mLayoutStreamState = LSS_START;
    mLayoutStream->print(APP_CMD_PREFIX);
    mLayoutStream->print(APP_CMD_LAYOUT_DEF);
    StartContainer(-1, true, false, 0, -1, true);
  }
  else
  {
    DEBUG_PRINTLN_STRING_CONST("ERROR: calling RobotUI::StartLayout twice");
  }
}

void RobotUI::EndLayout()
{
  if(mLayoutDefMode)
  {
    EndVertical();
    mLayoutDefMode = false;
    mLayoutStream->println(APP_CMD_POSTFIX);
    mLayoutStream->print('\0');
  }
  else
  {
    DEBUG_PRINTLN_STRING_CONST("ERROR: calling RobotUI::EndLayout but RobotUI::StartLayout has not been called");
  }
}

void RobotUI::AddSpace(float weight)
{
  Add(mSpaceWidget, weight, 0);
}

void RobotUI::AddGreySpace(float weight)
{
  Add(mGreySpaceWidget, weight, 0);
}

void RobotUI::AddLabel(const char* text, float weight, TextSize textSize, int justification)
{
  AddInternal(mSpaceWidget, weight, textSize, text, justification);
}

void RobotUI::Add(RobotUIWidget& yb, float weight, TextSize textSize, int justification)
{
  AddInternal(yb, weight, textSize, NULL, justification);
}

void RobotUI::AddInternal(RobotUIWidget& yb, float weight, TextSize textSize, const char* initialText, int justification)
{
  if(mLayoutDefMode)
  {
    yb.AppendWidgetAsJSONStart(*mLayoutStream, weight, textSize, initialText, justification, *this);
    yb.AppendWidgetAsJSONEnd(*mLayoutStream, *this);
  }
  else
  {
    DEBUG_PRINTLN_STRING_CONST("ERROR: calling RobotUI::Add before RobotUI::StartLayout");
  }
}

void RobotUI::StartVertical(float weight)
{
  StartContainer(-1, true, false, weight, -1, true);
}

void RobotUI::StartVertical(float weight, RobotUICheckBox& toggleWidget, bool visWhenTrue)
{
  StartContainer(-1, true, false, weight, toggleWidget.GetWidgetIndexAsInt(), visWhenTrue);
}

void RobotUI::StartVertical(float weight, RobotUIToggleButton& toggleWidget, bool visWhenTrue)
{
  StartContainer(-1, true, false, weight, toggleWidget.GetWidgetIndexAsInt(), visWhenTrue);
}

void RobotUI::EndVertical()
{
  EndContainer(true, false);
}

void RobotUI::StartHorizontal(float weight)
{
  StartContainer(-1, false, false, weight, -1, true);
}

void RobotUI::StartHorizontal(float weight, RobotUICheckBox& toggleWidget, bool visWhenTrue)
{
  StartContainer(-1, false, false, weight, toggleWidget.GetWidgetIndexAsInt(), visWhenTrue);
}

void RobotUI::StartHorizontal(float weight, RobotUIToggleButton& toggleWidget, bool visWhenTrue)
{
  StartContainer(-1, false, false, weight, toggleWidget.GetWidgetIndexAsInt(), visWhenTrue);
}

void RobotUI::EndHorizontal()
{
  EndContainer(false, false);
}

void RobotUI::StartVerticalScroll(float weight)
{
  StartContainer(-1, true, true, weight, -1, true);
}

void RobotUI::EndVerticalScroll()
{
  EndContainer(true, true);
}

void RobotUI::StartHorizontalScroll(float weight)
{
  StartContainer(-1, false, true, weight, -1, true);
}

void RobotUI::EndHorizontalScroll()
{
  EndContainer(false, true);
}

void RobotUI::StartContainer(int widAsInt, bool vertical, bool scrollable, float weight, int toggleWidgetIndex, bool visWhenTrue)
{
  if(mCurrentContDepth<MAX_CONTAINER_DEPTH)
  {
    char charCode = vertical?(scrollable?CODE_VERT_SCROLL:CODE_VERT):(scrollable?CODE_HORIZ_SCROLL:CODE_HORIZ);
    int toggleWidgetParam = visWhenTrue?1:0;
    RobotUIWidget::AppendWidgetAsJSONStart(
      *mLayoutStream, 
      charCode,
      widAsInt,
      weight,
      DEFAULT_TEXT_SIZE,
      NULL,
      DEFAULT_JUSTIFY,
      toggleWidgetIndex,
      toggleWidgetParam,
      *this);
    mCurrentContVertical[mCurrentContDepth] = vertical;
    mCurrentContScrollable[mCurrentContDepth] = scrollable;
    mCurrentContDepth++;
  }
}

void RobotUI::EndContainer(bool vertical, bool scrollable)
{
  --mCurrentContDepth;
  bool currentVertical = mCurrentContVertical[mCurrentContDepth];
  bool currentScrollable = mCurrentContScrollable[mCurrentContDepth];
  RobotUIWidget::AppendWidgetAsJSONEnd(*mLayoutStream, *this);
  if((currentVertical != vertical) || (currentScrollable != scrollable))
  {
    if(scrollable)
    {
      if(vertical)
      {
        DEBUG_PRINT_STRING_CONST("RobotUI.EndVerticalScrollable called when ");
      }
      else
      {
        DEBUG_PRINT_STRING_CONST("RobotUI.EndHorizontalScrollable called when ");
      }
    }
    else
    {
      if(vertical)
      {
        DEBUG_PRINT_STRING_CONST("RobotUI.EndVertical called when ");
      }
      else
      {
        DEBUG_PRINT_STRING_CONST("RobotUI.EndHorizontal called when ");
      }
    }
    if(currentScrollable)
    {
      if(currentVertical)
      {
        DEBUG_PRINTLN_STRING_CONST("EndVerticalScrollable was expected");  
      }
      else
      {
        DEBUG_PRINTLN_STRING_CONST("EndHorizontalScrollable was expected");  
      }
    }
    else
    {
      if(currentVertical)
      {
        DEBUG_PRINTLN_STRING_CONST("EndVertical was expected");  
      }
      else
      {
        DEBUG_PRINTLN_STRING_CONST("EndHorizontal was expected");  
      }
    }
  }
}

int RobotUI::GetStreamState()
{
  return mLayoutStreamState;
}

void RobotUI::SetStreamState(int newState)
{
  mLayoutStreamState = newState;
}

void RobotUI::CommandAppend(const char* cmd)
{
  if((mInstance!=NULL) && (cmd!=NULL))
  {
    mInstance->CommandBufferAppend(cmd);
  }
}

void RobotUI::CommandAppend(int cmd)
{
  if(mInstance!=NULL)
  {
    mInstance->CommandBufferAppend(cmd);
  }
}

void RobotUI::CommandStart()
{
  if(mInstance!=NULL)
  {
    mInstance->CommandBufferStartCMD();
  }
}

void RobotUI::CommandEnd()
{
  if(mInstance!=NULL)
  {
    mInstance->CommandBufferEndCMD();
  }
}

void RobotUI::CommandBufferStartCMD()
{
  mPartialCommandInProgress = true;
  mCommandBuffer.Append(APP_CMD_PREFIX);
}

void RobotUI::CommandBufferEndCMD()
{
  mPartialCommandInProgress = false;
  mCommandBuffer.Append(APP_CMD_POSTFIX);
  if(mCommandBuffer.Size()>(DEFAULT_COMMAND_BUFFER_SIZE>>1)) // NJM - if the command buffer is half full, flush it
  {
    FlushAppCommands();
  }
}

void RobotUI::CommandBufferAppend(const char* cmd)
{
  mPartialCommandInProgress = true;
  if(!mCommandBuffer.Append(cmd))
  {
    DEBUG_PRINT_STRING_CONST("Command buffer overrun. Increase size above ");
    DEBUG_PRINTLN_VAR(DEFAULT_COMMAND_BUFFER_SIZE);
  }
}

void RobotUI::CommandBufferAppend(int num)
{
  mPartialCommandInProgress = true;
  if(!mCommandBuffer.Append(num))
  {
    DEBUG_PRINT_STRING_CONST("Command buffer overrun. Increase size above ");
    DEBUG_PRINTLN_VAR(DEFAULT_COMMAND_BUFFER_SIZE);
  }
}

