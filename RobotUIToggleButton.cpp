#include "RobotUIToggleButton.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

RobotUIToggleButton::RobotUIToggleButton(TextSize textSize)
{
  mEnabled = true;
  mIsPressed = false;
  mHasChanged = false;
  mTextSize = textSize;
}

void RobotUIToggleButton::SetNames(const char* newOnName, const char* newOffName)
{
  if((newOnName != NULL) && (newOffName != NULL))
  {
    RobotUI::CommandStart();
    RobotUI::CommandAppend(APP_CMD_SET_DUAL_NAME);
    RobotUI::CommandAppend(GetWidgetIndexAsInt());
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(newOnName);
    RobotUI::CommandAppend("}{");
    RobotUI::CommandAppend(newOffName);
    RobotUI::CommandAppend("}");
    RobotUI::CommandEnd();
  }
}

void RobotUIToggleButton::SetNames(const __FlashStringHelper* newOnName, const __FlashStringHelper* newOffName)
{
  if((newOnName != NULL) && (newOffName != NULL))
  {
    const int bufferSize = 64;
    char charBufferOn[bufferSize];
    char charBufferOff[bufferSize];
    strncpy_P(charBufferOn, (const char*)newOnName, bufferSize-1);
    strncpy_P(charBufferOff, (const char*)newOffName, bufferSize-1);
    charBufferOn[bufferSize-1] = 0;
    charBufferOff[bufferSize-1] = 0;
    SetNames(charBufferOn, charBufferOff);
  }
}

bool RobotUIToggleButton::IsPressed()
{
  return mIsPressed;
}

void RobotUIToggleButton::SetIsPressed(bool pressed)
{
  SetIsPressedInternal(pressed, false, false);
}

void RobotUIToggleButton::ForceIsPressed(bool pressed)
{
  SetIsPressedInternal(pressed, false, true);
}

void RobotUIToggleButton::SetIsPressedFromApp(bool pressed)
{
  SetIsPressedInternal(pressed, true, false);
}

void RobotUIToggleButton::SetIsPressedInternal(bool pressed, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if(mIsPressed != pressed)
  {
    mIsPressed = pressed;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUIToggleButton::SetIsPressedEnabled(bool pressed, bool enabled)
{
  SetIsPressedEnabledInternal(pressed, enabled, false, false);
}

void RobotUIToggleButton::SetIsPressedEnabledInternal(bool pressed, bool enabled, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if((mIsPressed != pressed) || (mEnabled != enabled))
  {
    mIsPressed = pressed;
    mEnabled = enabled;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUIToggleButton::SendState()
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_TOGGLE_BUTTON);
  RobotUI::CommandAppend(mIsPressed?"1":"0");
  RobotUI::CommandAppend(mEnabled?"1":"0");
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandEnd();
}


void RobotUIToggleButton::ConsumeHasChanged()
{
  mHasChanged = false;
}

bool RobotUIToggleButton::HasChanged()
{
  return mHasChanged;
}

