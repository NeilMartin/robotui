#include "RobotUIButton.h"
#include "RobotUISwitch.h"
#include "RobotUI.h"
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

RobotUISwitch::RobotUISwitch(TextSize textSize)
{
  mIsPressed = false;
  mHasChanged = false;
  mEnabled = true;
  mTextSize = textSize;
}

void RobotUISwitch::SetName(const char* newName)
{
  if(newName != NULL)
  {
    RobotUI::CommandStart();
    RobotUI::CommandAppend(APP_CMD_SET_NAME);
    RobotUI::CommandAppend(GetWidgetIndexAsInt());
    RobotUI::CommandAppend("{");
    RobotUI::CommandAppend(newName);
    RobotUI::CommandAppend("}");
    RobotUI::CommandEnd();
  }
}

void RobotUISwitch::SetName(const __FlashStringHelper* newName)
{
  if(newName != NULL)
  {
    const int bufferSize = 64;
    char charBuffer[bufferSize];
    strncpy_P(charBuffer, (const char*)newName, bufferSize-1);
    charBuffer[bufferSize-1] = 0;
    SetName(charBuffer);
  }
}

bool RobotUISwitch::IsPressed()
{
  return mIsPressed;
}

void RobotUISwitch::SetIsPressed(bool pressed)
{
  SetIsPressedInternal(pressed, false, false);
}

void RobotUISwitch::ForceIsPressed(bool pressed)
{
  SetIsPressedInternal(pressed, false, true);
}

void RobotUISwitch::SetIsPressedFromApp(bool pressed)
{
  SetIsPressedInternal(pressed, true, false);
}

void RobotUISwitch::SetIsPressedInternal(bool pressed, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if(mIsPressed != pressed)
  {
    mIsPressed = pressed;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUISwitch::SetIsPressedEnabled(bool pressed, bool enabled)
{
  SetIsPressedEnabledInternal(pressed, enabled, false, false);
}

void RobotUISwitch::SetIsPressedEnabledInternal(bool pressed, bool enabled, bool commandFromApp, bool forceSend)
{
  boolean shouldSend = false;
  if((mIsPressed != pressed) || (mEnabled != enabled))
  {
    mIsPressed = pressed;
    mEnabled = enabled;
    mHasChanged = true;
    if(!commandFromApp)
    {
      shouldSend = true;
    }
  }
  if(shouldSend || forceSend)
  {
    SendState();
  }
}

void RobotUISwitch::SendState()
{
  RobotUI::CommandStart();
  RobotUI::CommandAppend(APP_CMD_SET_SWITCH);
  RobotUI::CommandAppend(mIsPressed?"1":"0");
  RobotUI::CommandAppend(mEnabled?"1":"0");
  RobotUI::CommandAppend(GetWidgetIndexAsInt());
  RobotUI::CommandEnd();
}


void RobotUISwitch::ConsumeHasChanged()
{
  mHasChanged = false;
}

bool RobotUISwitch::HasChanged()
{
  return mHasChanged;
}

